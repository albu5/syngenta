
%%
clear all
clc
%%

fileToRead = 'Experiment.xlsx';
[num, txt, raw] = xlsread(fileToRead, 1);
yield = num(:,4);


%%
[l,b] = size(num);

feature_matrix = zeros(l,b-7);
feature_matrix(:,1:2) = num(:,5:6);
feature_matrix(:,3:end) = num(:,10:end);


%% linear normalization

[~,b2] = size(feature_matrix);
fm_normal = zeros(size(feature_matrix));

for i = 1:b2
    temp = feature_matrix(:,i);
    temp1 = max(temp);
    temp2 = min(temp);
    for j = 1:l
        fm_normal(j,i) = (feature_matrix(j,i) - temp2)/(temp1 - temp2);
    end
end

%%
fmn_train = fm_normal(1:0.75*l,:) ;
fmn_test = fm_normal(0.75*l+1:end,:);

[b,bint,r] = regress(yield(1:0.75*l,:),fmn_train);

y = fmn_test*b;
err = y-yield(0.75*l+1:end,:);

%%

%err = norm(err);
sum_err = 0;
count = 0;

for i = 1:length(err)
    if (isnan(err(i)))
        continue;
    else
        sum_err = sum_err + (err(i))^2;
        count = count + 1;
    end
end

sum_err

        
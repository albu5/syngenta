load Experiment.mat
uniqueVariety = unique(Variety);

%%
Predictors = horzcat(Area, CEC, Clay, Organicmatter, pH, PI, ...
        Precipitation, Sand, Silt, SolarRadiation, Temperature, Year);
    
for variety = uniqueVariety'
    % analyzing each variety individually
    %%
    % indices of experiments with the selected variety
    idx = strcmp(Variety, variety);
    % scatter plot of yeild vs precipitation
    % scatter(Yield(idx), Precipitation(idx));
    % pause
    
    %%
    yield = Yield(idx);
    predictors = Predictors(idx,:);
    predictors = horzcat(ones(size(predictors,1),1), predictors);
    [B, Bint, Res] = regress(yield, predictors);
    %%
    % residual plot not suggesting much
    
    scatter(predictors*B, Res);
    xlabel('predicted yield')
    ylabel('residual vale')
    % r sq value also suggests a bad fit
    title(sprintf('Variety %s : R square value of regression fit is %f', ...
        char(variety), 1-nansum(Res.*Res)/(var(yield)*numel(Res))));
    figure
    scatter(yield, predictors*B);
    title('Predicted value vs actual value');
    pause
    close all
    
end